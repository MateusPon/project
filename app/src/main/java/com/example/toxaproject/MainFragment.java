package com.example.toxaproject;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.toxaproject.adapters.MainListAdapter;
import com.example.toxaproject.models.Person;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private ArrayList<Person> persons;
    private RecyclerView list;
    private MainListAdapter adapter;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        persons = new ArrayList<>();
        persons.add(new Person("Anton", "Brevnov"));
        persons.add(new Person("Matvey", "Ponomarev"));
        persons.add(new Person("Margarita", "Mulkeeva"));

        list = view.findViewById(R.id.my_recycle_view);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        adapter = new MainListAdapter(persons);
        list.setAdapter(adapter);
        list.setLayoutManager(manager);

        return view;
    }
}
package com.example.toxaproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.toxaproject.R;
import com.example.toxaproject.models.Person;

import java.util.List;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.PersonHolder> {

    private final List<Person> persons;

    public MainListAdapter(List<Person> persons) {
        this.persons = persons;
    }

    @NonNull
    @Override
    public PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_list_item, parent, false);
        return new PersonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonHolder holder, int position) {
        Person person = persons.get(position);
        holder.nameText.setText(person.getName());
        holder.surnameText.setText(person.getSurname());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public class PersonHolder extends RecyclerView.ViewHolder {
        final TextView nameText;
        final TextView surnameText;

        public PersonHolder(@NonNull View itemView) {
            super(itemView);
            nameText = itemView.findViewById(R.id.person_name);
            surnameText = itemView.findViewById(R.id.person_surname);
        }
    }
}

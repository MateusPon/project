package com.example.toxaproject.Screens;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.toxaproject.R;
import com.example.toxaproject.models.SignInRequestBody;
import com.example.toxaproject.models.SignInResponseBody;
import com.example.toxaproject.network.ApiHandler;
import com.example.toxaproject.network.ErrorUtils;
import com.example.toxaproject.network.IApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity {

    private EditText etFio, etPassword;

    private IApiService service = ApiHandler.getInstance().getService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        etFio = findViewById(R.id.txtEditEmail);
        etPassword = findViewById(R.id.txtEditPassword);

    }

    public void goMain(View view) {
        AsyncTask.execute(() ->{
            service.doSignIn(new SignInRequestBody(etFio.getText().toString(),etPassword.getText().toString())).enqueue(new Callback<SignInResponseBody>() {
                @Override
                public void onResponse(Call<SignInResponseBody> call, Response<SignInResponseBody> response) {
                    if(response.isSuccessful()){
                        Toast.makeText(getApplicationContext(), response.body().getEmail(),Toast.LENGTH_SHORT).show();
                    }else{
                        String serverErrorMessage = ErrorUtils.parseError(response).message();
                        Toast.makeText(getApplicationContext(), serverErrorMessage.toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SignInResponseBody> call, Throwable t) {

                }
            });
        });
    }

    public void goRegistration(View view) {
    }
}
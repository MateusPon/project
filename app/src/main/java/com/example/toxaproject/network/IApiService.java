package com.example.toxaproject.network;

import com.example.toxaproject.models.SignInRequestBody;
import com.example.toxaproject.models.SignInResponseBody;
import com.example.toxaproject.models.SignUpRequestBody;
import com.example.toxaproject.models.SignUpResponseBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IApiService {
    @POST("users/login")
    Call<SignInResponseBody> doSignIn(@Body SignInRequestBody signInRequestBody);

    @POST("users/registration")
    Call<SignUpResponseBody> doSingUp(@Body SignUpRequestBody signUpRequestBody);
}

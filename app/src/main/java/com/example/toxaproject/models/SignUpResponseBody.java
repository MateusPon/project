package com.example.toxaproject.models;

import com.google.gson.annotations.SerializedName;

public class SignUpResponseBody {

    @SerializedName("id")
    private long id;

    @SerializedName("fio")
    private String fio;

    @SerializedName("password")
    private String password;

    @SerializedName("email")
    private String email;


    @SerializedName("position_id")
    private String position_id;

    public SignUpResponseBody(long id, String fio, String password, String email, String position_id) {
        this.id = id;
        this.fio = fio;
        this.password = password;
        this.email = email;
        this.position_id = position_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition_id() {
        return position_id;
    }

    public void setPosition_id(String position_id) {
        this.position_id = position_id;
    }

}
